'use strict';


var mongoose = require('mongoose'),
  Ressources = mongoose.model('DR');

exports.list_all_ressource = function(req, res) {
  Ressources.find({}, function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};

exports.new_ressource = function(req, res) {
  var new_task = new Ressources(req.body);
  new_task.save(function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};


exports.get_ressource = function(req, res) {
  Ressources.findById(req.params.taskId, function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};


exports.update_a_task = function(req, res) {
  Ressources.findOneAndUpdate({_id: req.params.taskId}, req.body, {new: true}, function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};


exports.delete_a_task = function(req, res) {


  Ressources.remove({
    _id: req.params.taskId
  }, function(err, task) {
    if (err)
      res.send(err);
    res.json({ message: 'Task successfully deleted' });
  });
};
