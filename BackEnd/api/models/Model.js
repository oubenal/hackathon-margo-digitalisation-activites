'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var RessourceSchema = new Schema({
  reference: {
    type: String,
    required: 'Kindly enter demolition site reference'
  },
  picture_url: {
    type: String,
    default: ''
  },
  amount: {
    type: Number,
    default: '1'
  },
  Created_date: {
    type: Date,
    default: Date.now
  },
  material: {
    type: [{
      type: String,
      enum: ['Fer', 'Inox', 'Bois', 'Plastic', 'Autre']
    }],
    default: ['Autre']
  }
});

module.exports = mongoose.model('DR', RessourceSchema);