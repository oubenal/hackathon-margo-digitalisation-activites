'use strict';
module.exports = function(app) {
  var dataDB = require('../controllers/Controller');

  // todoList Routes
  app.route('/DR')
    .get(dataDB.list_all_ressource)
    .post(dataDB.new_ressource);


  app.route('/DR/:ref')
    .get(dataDB.get_ressource)
    .put(dataDB.update_a_task)
    .delete(dataDB.delete_a_task);
};