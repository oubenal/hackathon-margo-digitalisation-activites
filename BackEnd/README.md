# RESTful Web API with Node.js Framework

### Tools
- Node.js
(How to instal url : `https://nodejs.org/en/download/package-manager/`)
- MongoDB
(How to instal url : `https://docs.mongodb.com/manual/installation/`)

### Node Version
>`node -v`

`v8.10.0`

### Framework used:
Express.js https://expressjs.com/ 

### Install dependecies
in your terminal run:
> `npm install`
### Run the project
with:
>`npm start`
### Run test
>`npm test`

### Endpoint documentation
#### Get all available ressources:
- url: ` http://localhost:3000/DB`
- Http method: GET

##### examples: 
- New valid request
`curl -X GET "http://localhost:3000/DB"`

response: `[
    {
        "picture_url": "https://i.ibb.co/61Zjc23/ax-Nn-E1-Xb-700w-0.jpg",
        "amount": 2,
        "material": [
            "Inox"
        ],
        "_id": "5c680413bb042814d4849f56",
        "reference": "read nodejs server ",
        "Created_date": "2019-02-16T12:37:39.497Z",
        "__v": 0
    },
    {
        "picture_url": "https://i.ibb.co/61Zjc23/ax-Nn-E1-Xb-700w-0.jpg",
        "amount": 10,
        "material": [
            "Plastic",
            "Inox"
        ],
        "_id": "5c680502bb042814d4849f5d",
        "reference": "ANT-CLA-CU-002",
        "Created_date": "2019-02-16T12:41:38.070Z",
        "__v": 0
    }
]`

#### Add a ressource:
- url: ` http://localhost:3000/DB`
- Http method: POST
- Data: {"reference": "<REFERENCE>", "picture_url": "<PICTURE_URL>", "amount": "<AMOUNT>", "material": "[<MATERIAL1>, <MATERIAL2>, ...]"}
	+ <MATERIAL> should be one from enum

##### examples: 
- `curl -X POST "http://localhost:3000/DB" -H 'Content-Type: application/json' -d $'{"reference": "ANT-CLA-CU-002", "picture_url": "https://i.ibb.co/61Zjc23/ax-Nn-E1-Xb-700w-0.jpg"}'`

response: `{
    "picture_url": "https://i.ibb.co/61Zjc23/02.jpg",
    "amount": 10,
    "material": [
        "Plastic",
        "Inox"
    ],
    "_id": "5c680743bb042814d4849f5e",
    "reference": "ANT-CLA-CU-002",
    "Created_date": "2019-02-16T12:51:15.560Z",
    "__v": 0
}`